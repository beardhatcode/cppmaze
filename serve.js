#!/usr/bin/node
var sys = require('util'),
    exec = require('child_process').exec ,
    http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs")
    port = process.argv[2] || 8888;


function showError(response, code, error){
  response.writeHead(code, {"Content-Type": "text/plain"});
  response.write(error+"\n");
  response.write("To use the servide use http[s]://example.com/{width}/{height}/\n"+
                 "Where {width} and {height} are positive integers larger than 5 and with a product lower than 50000");
  response.end();
}

http.createServer(function(request, response) {

  var uri = url.parse(request.url).pathname.split("/")

  if(uri.length != 4 || uri[0] != ""){
    showError(response, 404, "Not found")
    return;
  }
    var w = parseInt(uri[1], 10),
        h = parseInt(uri[2], 10);

  if(uri[1] != ""+w || uri[2] != ""+h ){
    showError(response, 400, "Bad request");
    return;
  }

  if(w*h >50000){
    showError(response, 403, "Request denied");
    return;
  }

  //Execute mazegenerator
  exec("./MazeGenerator "+w+" "+h, function (error, stdout, stderr) {
    response.writeHead(200, {"Content-Type": "text/plain","charset": "utf-8"});
    response.write('stdout: \n' + stdout,"utf-8");
    response.write('stderr: \n' + stderr);
    if (error !== null) {
      console.log('exec error: ' + error);
    }
    response.end();
  });

}).listen(parseInt(port, 10));

console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
