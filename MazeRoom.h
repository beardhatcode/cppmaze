//
// Class representing a Ropm in the maze
// Created by garonn on 12/09/15.
//

#ifndef CPPREFRESH_MAZEROOM_H
#define CPPREFRESH_MAZEROOM_H


#include "MazeModifier.h"
#include "Maze.h"

/**
 * @brief A room in the maze
 */
class MazeRoom : public MazeModifier {
    int x;
    int y;
    int h;
    int w;
public:

/**
 * @brief Constructor for a room in the maze
 *
 * @param x X start pos
 * @param y Y start pos
 * @param h height (end pos: x+h-1)
 * @param w width (end pos: y+w-1)
 */
    MazeRoom(int x,int y,int h,int w):x(x),y(y),h(h),w(w) { }

    /**
     * @brief set's the area of the maze to occupied
     */
    virtual void preGeneration(Maze *maze);


    virtual void postGeneration(Maze *maze);

};


#endif //CPPREFRESH_MAZEROOM_H
