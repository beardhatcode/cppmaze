//
// Created by Robbert Gurdeep Singh on 06/09/15.
//

#include <iomanip>
#include "Maze.h"
#include "BingoSet.h"
#include "MazeModifier.h"



using namespace std;

inline size_t Maze::is_used(size_t x, size_t y){
    if (x >= 0 && y >= 0 && x < height && y < width) {
        return matrix[x][y].path;
    }
    else {
        //out of range == path
        return 1;
    }
}

Maze::Maze(size_t height, size_t width) {
    this->height= height;
    this->width = width;


    //Initialize matrix
    matrix = new MazePos*[height];
    matrix[0] = new MazePos[height * width]{};//init all to 0
    for (size_t i = 1; i < height; ++i) {
        matrix[i] = matrix[0]+i* width;
    }
}

void Maze::generate() {
    //Set that contains existing paths that havn't been used as starting point yet
    BingoSet<std::pair <size_t,size_t>> set;

    //Starting point is the center
    size_t i = height /2;
    size_t j= width /2;

    //i2 and j2 store suggestions for the next path coords
    size_t i2=i;
    size_t j2=j;

    size_t num_placed = 0;


    size_t neigh_direct = 0;
    size_t neigh_diag = 0;
    bool list_empty = false;

    while(num_placed < height * width * 2){
        size_t tries = 0;
        do {
            if(tries++ > 4) {
                //If can't find a good place to continue: start from other place
                std::pair <size_t,size_t> newStart;
                if(set.pop(newStart) < 0){ /*List empty:*/  list_empty=true;  break;}
                i = newStart.first;
                j= newStart.second;
                tries = 0;
            }

            //Randomly select a neighbour
            i2=i;j2=j;
            if(RANDBOOL()) {
                i2 += RANDSIGN();
            } else {
                j2 += RANDSIGN();
            }

            neigh_direct = is_used(i2 - 1, j2)+ is_used(i2 + 1, j2)+ is_used(i2, j2 - 1)+ is_used(i2, j2 + 1) + is_used(i2, j2);
            neigh_diag   = is_used(i2 - 1, j2 - 1) + is_used(i2 + 1, j2 - 1) + is_used(i2 - 1, j2 + 1) + is_used(i2 + 1, j2 + 1);

            // Try an other point if the neighbour already has more than 2 neighbours
            // or if it has more than on diagonal neighbour.
        }while (neigh_direct > 2 || neigh_diag > 1 || is_used(i2, j2) || matrix[i2][j2].occupied);

        i=i2;
        j=j2;
        if(list_empty) break; //no more points to start from, we are done

        //The current point is good
        matrix[i][j].path = 1;
        set.push(make_pair(i, j));
        num_placed++;

    }

    for(MazeModifier* mod : modifiers){
        mod->postGeneration(this);
    }
}

//░ ▒ ▓
ostream & operator<<(ostream &stream, const Maze &matrix) {
    stream << "Maze of " << matrix.width << " by " << matrix.height << endl;
    stream << setw(4)<< " ";
    for (size_t j = 0; j <matrix.width; ++j) {
        stream << setw(2)<< j % 100;
    }
    stream << endl;

    for (size_t i = 0; i <matrix.height; ++i) {
        stream << setw(4)<< i;
        for (size_t j = 0; j <matrix.width; ++j) {
            MazePos &pos = matrix.matrix[i][j];
            stream << (pos.path ? "##" : (pos.occupied ? "@@" : "  ")) ;
        }

        stream << endl;
    }
    return stream;
}

void Maze::addModifier(MazeModifier &modifier) {
    modifier.preGeneration(this);
    modifiers.push_back(&modifier);
}


