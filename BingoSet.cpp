//
// Created by garonn on 10/09/15.
//

#include "BingoSet.h"

template <class T>
void BingoSet<T>::push(T t) {
    array.push_back(t);
}

template <class T>
int BingoSet<T>::pop(T &result) {
    unsigned long size = array.size();
    if(size==0)
        return -1;

    unsigned long index = rand() % size;
    result = array[index];
    array[index] = array[size-1];
    array.pop_back();
    return  1;
}

template <class T>
unsigned long BingoSet<T>::size() {
    return array.size();
}
