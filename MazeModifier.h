//
// Class representing a MazeModifier (First in sometime out)
// Created by garonn on 12/09/15.
//

#ifndef CPPREFRESH_MAZEMOD_H
#define CPPREFRESH_MAZEMOD_H

class Maze;    //Forward Decl.: Needs to be applied to a Maze
class MazePos; //Forward Decl.: To retun

/**
 * @brief A MazeModifier modifies a maze with rooms, paths, ...
 *
 * A modifier can change the maze before and after the generation
 * process. By occupyingg" certain spots we can ensure that the
 * maze generator wil not place paths there. In the postGeration step.
 * You may want to check if this place is connected to maze.
 */
class MazeModifier {
public:
/**
 * @brief Modifier constructor... Not used because abstract
 */
    MazeModifier() { }

    /**
    * @brief Stuff to do before generation of the maze
    */
    virtual void preGeneration(Maze *maze) = 0;
    /**
    * @brief Stuff to do after generation of the maze
    */
    virtual void postGeneration(Maze *maze) = 0;

protected:
    /**
    * @brief Method that can be called from child classes to axes the matrix
    */
    MazePos** getMazeMatrix(Maze *maze);
};


#endif //CPPREFRESH_MAZEMOD_H