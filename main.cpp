#include <iostream>
#include "Maze.h"
#include "MazeRoom.h"

using namespace std;

int main(int argc, char *argv[]) {

    if(argc != 3){
        cerr << "Please provide 2 dimensions";
        return -1;
    }
    int w_u =  atoi(argv[1]);
    int h_u =  atoi(argv[2]);

    if(1000000 <= abs((long)w_u * (long)h_u)){
        cerr << "maze too large!";
        return -1;
    }

    if(w_u < 5 || h_u < 5){
        cerr << "maze Should be at least 5x5!";
        return -1;
    }

    size_t w = (size_t) w_u;
    size_t h = (size_t) h_u;


    srand ((unsigned int) time(NULL));

    Maze  maze(h, w);
    if(w > 50 && h > 50){
        //Add some cool rooms
        MazeRoom room1(1, 1, h / 5, w / 5);
        maze.addModifier(room1);

        MazeRoom room2(3*h/5,w/5, h/5,w/5);
        maze.addModifier(room2);

        MazeRoom room3(2*h/6,4*w/5, h/5,w/6);
        maze.addModifier(room3);
    }
    else
    {
        if(w > 20 && h > 20) {
            //Add a lame room
            MazeRoom room(1, 1, h / 5, w / 5);
            maze.addModifier(room);
        }
    }
    maze.generate();
    cout << maze;
    return 0;
}