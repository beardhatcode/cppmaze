Data structures for random sampling
===================================

Requirements
------------
Our data structure should allow us to perform following actions
efficiently:

 - Addition to the data set
 - Removal of a **random** element

 
Solution
--------

  - Exponentially growing array 
  - Random pop by selecting random index and filling hole with
    last element.

Complexity
-----------
###Addition
An addition to an exponentially growing array is (amortized) O(1).
###Random removal
To randomly take an element out of the list the flowing
algorthm is used:

1. Select a random index
2. Place the last element in that position
3. Return a copy of the original element.

This action always requires O(1).