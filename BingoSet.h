//
// Class representing a FISO Queue (First in sometime out)
// Created by Robbert Gurdeep Singh on 06/09/15.
//

#ifndef CPPREFRESH_BINGOSET_H
#define CPPREFRESH_BINGOSET_H

#include <vector>
#include "Maze.h"

/**
 * @brief A set out of which we can randomly select an element in constant time
 *
 * Let's call it a FISO Queue (First in sometime out)
 *
 * @tparam T the type of the content
 */
template <typename T>
class BingoSet {
    std::vector<T> array;
public:
  /**
   * @brief Constructor initialize
   */
    BingoSet() { }

  /**
   * @brief add an element to the set
   */
    void push(T);

  /**
   * @brief pop a random element from the set
   */
    int pop(T &result);

    /**
     * @brief returs the size set
     */
    unsigned long size();
};
//TODO: Realisations probably don't belong in the header, I think...
template class BingoSet<MazePos>;
template class BingoSet<std::pair <size_t,size_t>>;
template class BingoSet<size_t>;
#endif //CPPREFRESH_BINGOSET_H
