//
// Created by garonn on 12/09/15.
//

#include "MazeRoom.h"

void MazeRoom::preGeneration(Maze *maze) {
    for (size_t i = x; i < x+h; ++i) {
        for (size_t j = y; j < y+w; ++j) {
            getMazeMatrix(maze)[i][j].occupied = 1;
        }
    }
}

void MazeRoom::postGeneration(Maze *maze) {

    //Make outer border of room a path
    for (size_t i = x; i < x+h; ++i) {
        getMazeMatrix(maze)[i][y].occupied = 0;
        getMazeMatrix(maze)[i][y].path = 1;
        getMazeMatrix(maze)[i][y+w-1].occupied = 0;
        getMazeMatrix(maze)[i][y+w-1].path = 1;
    }
    for (size_t j = y; j < y+w; ++j) {
        getMazeMatrix(maze)[x][j].occupied = 0;
        getMazeMatrix(maze)[x][j].path = 1;
        getMazeMatrix(maze)[x+h-1][j].occupied = 0;
        getMazeMatrix(maze)[x+h-1][j].path = 1;
    }

    //Check if the room is connected to the maze

}
