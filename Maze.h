//
// Class representing a Maze
// Created by Robbert Gurdeep Singh on 06/09/15.
//

#ifndef CPPREFRESH_MAZE_H
#define CPPREFRESH_MAZE_H

#include <iostream>        //for: ostream
#include <vector>          //for: vector
#include <set>             //for: set

#define RANDSIGN()       (rand()%2 ? -1 : 1)
#define RANDBOOL()       (rand()%2)

class MazeModifier; //Forward definition


// A place in the maze
struct MazePos {
    unsigned occupied  : 1;
    unsigned path: 1;
};

/**
 * @brief A class representing and generatng a maze based on
 * a size and a set of modifiers
 *
 * *NOTE* Maze is **STATEFULL**.
 */
class Maze {
    size_t width, height;
    MazePos** matrix;
    std::vector<MazeModifier*> modifiers;
public:
 /**
  * @brief Constructor of a maze
  *
  * @param h the height of the maze
  * @param w the width of the maze
  */
    Maze (size_t h,size_t w);

/**
 * @brief Generate a maze based on the dimensions and modifiers
 *
 * Should be called  AFTER all modifiers have been added
 */
    void generate();
/**
 * @brief Add a modifier to the maze
 *
 * Will execute the preGeneration() on the maze.
 * MazeModifier is a friend class, will be able to access the matrix
 */
    void addModifier(MazeModifier &modifier);
    friend class MazeModifier;

/**
 * @brief A toString for cli visualisation
 *
 * Should not be used to perform logic on, only for
 * cli output
 */
    friend std::ostream& operator<< (std::ostream& stream, const Maze& matrix);

private:

/**
 * @brief 1 if (x,y) is used in the maze  (occupied or path or out of bound)
 */
    size_t is_used(size_t x, size_t y);
};



#endif //CPPREFRESH_MAZE_H
